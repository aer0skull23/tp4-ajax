<?php

use Slim\Http\Request;
use Slim\Http\Response;

//back
$app->post('/login', function (Request $request, Response $response, array $args) {

    $id = $request->getParam('login');
    $pass = $request->getParam('password');
    $token = $request->getParam('token');

    $ret = [];

    if ($id == "test" && $pass == "test" && $_SESSION["token"] == $token) {
        $ret["status"] = "success";
        $ret["message"] = "you are logged in";
    } else {
        $ret["status"] = "error";
        $ret["error"] = "username";
        $ret["message"] = "unknown username";
    }


    return $response->withJson($ret);
});


$app->post('/register', function (Request $request, Response $response, array $args) {
    return null;
});


$app->get('/getToken', function (Request $request, Response $response, array $args) {
    $salt = random_bytes(30);
    $token = hash("sha1", session_id() . $salt);

    $_SESSION["token"] = $token;


    return $response->withJson([
        "token" => $token
    ]);
});


//front
$app->get('/', function (Request $request, Response $response, array $args) {
    return $this->view->render($response, 'index.html.twig');
});
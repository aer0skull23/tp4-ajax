var origin, token;
var logedIn = false;
var alerts;


$(function () {
    alerts = $('#alerts');
    //get origin of url for requests
    origin = window.location.origin;
    //origin = "http://iutbelfort.ltg-services.fr";

    //load content for the first time
    reloadView();

    saveToken();
});

/**
 * get and save token for future usages
 */
function saveToken() {
    $.get(origin + '/getToken')
        .done(function (data) {
            token = data.token;
        })
        .fail(function (data) {
            clearAlerts();
            addFlash("danger", "unable to connect to the api")
        })
}

/**
 * Called when user click on the login button
 * @param username
 * @param password
 */
function login(username, password) {
    clearAlerts();
    $.post(origin + '/login', {
        "login": username,
        "password": password,
        "token": token
    })
        .done(function (data) {
            if (data.status === undefined) { //if I use teacher's API
                data = data[0];
            }

            if (data.status === "success") {
                logedIn = true;
                addFlash('success', data.message);
                reloadView()
            } else {
                logedIn = false;
                addFlash('danger', data.message);
            }
        })
        .fail(function () {
            addFlash("danger", "unable to connect to the api")
        });
}

/**
 * Called when user click on the register button
 * check if it's all good then send request
 * @param username
 * @param password
 * @param confirm
 */
function register(username, password, confirm) {
    clearAlerts();
    if ($('.is-valid').length !== 3) {
        if ($('.is-invalid').length === 0) {
            addFlash('danger', 'complete fields before register');
        } else {
            addFlash('danger', 'you have some errors left');
        }
        return;
    }

    $.post(origin + '/register', {
        "login": username,
        "password": password,
        "confirm": confirm,
        "token": token
    })
        .done(function (data) {
            //TODO
            console.log(data);
        })
        .fail(function (data) {
            addFlash("danger", "unable to connect to the api")
        })
}

/**
 * Clear all the flash messages displayed on the top
 */
function clearAlerts() {
    alerts.empty();
}

/**
 * Called after logged in or out to reload main content
 */
function reloadView() {
    let container = $('#container');
    let parentBtn = $('#navbarSupportedContent');

    //clear container
    container.empty();

    if (logedIn) {

        //add signout button from included template
        let template = $('#signOutBtnTemplate').html();
        Mustache.parse(template);   // optional, speeds up future uses
        let rendered = Mustache.render(template);
        parentBtn.html(rendered);

        //register event on sign out button
        $('#signOutBtn').on('click', function () {
            logedIn = false;
            clearAlerts();
            reloadView();
        });

        //add flickr stuff
        displayFlickr();

    } else { //if we are not logged in

        //remove sign out btn
        parentBtn.empty();

        //add main content from included template
        let template = $('#loggedOutTemplate').html();
        Mustache.parse(template);
        let rendered = Mustache.render(template);
        container.html(rendered);

        //sign in fields
        let loginInput = $('#loginInput');
        let pwdInput = $('#passwordInput');

        //register fields
        let loginRegister = $('#loginRegister');
        let pwdRegister = $('#passwordRegister');
        let pwdConfirm = $('#passwordConfirm');

        //register events
        $('#submitButton').on('click', function () {
            login(loginInput.val(), pwdInput.val());
        });

        $('#registerButton').on('click', function () {
            register(loginRegister.val(), pwdRegister.val(), pwdConfirm.val());
        });

        $('input').on('input', function () {
            editing($(this));
        });
    }
}

/**
 * Check for errors when the user edits fields
 * @param field
 */
function editing(field) {
    let id = field.attr('id');

    if (id === 'loginRegister') {
        if (field.val().length < 2) {
            field.removeClass('is-valid');
            field.addClass('is-invalid');
        } else {
            field.removeClass('is-invalid');
            field.addClass('is-valid')
        }
    }

    if (id === 'passwordRegister') {
        if (field.val().length < 8) {
            field.removeClass('is-valid');
            field.addClass('is-invalid');
        } else {
            field.removeClass('is-invalid');
            field.addClass('is-valid');
        }
    }

    if (id === 'passwordConfirm' || id === 'passwordRegister') {
        let f = $('#passwordConfirm');

        if (f.val() !== $('#passwordRegister').val()) {
            f.removeClass('is-valid');
            f.addClass('is-invalid');
        } else {
            f.removeClass('is-invalid');
            f.addClass('is-valid');
        }
    }
}

/**
 * display a flash message from the included template
 * @param type is the bootstrap type
 * @param message to be displayed
 */
function addFlash(type, message) {
    let template = $('#alertTemplate').html();
    Mustache.parse(template);
    let rendered = Mustache.render(template, {type: type, message: message});
    $('#alerts').append(rendered);
}

function displayFlickr() {
    $('#container').append('<section id="news" class="row"></section>');
    let template = $('#cardTemplate').html();
    let news = $('#news');
    Mustache.parse(template);


    let url = 'http://api.flickr.com/services/feeds/photos_public.gne?tags=besancon&tagmode=any&format=json&jsoncallback=?';
    $.ajax({
        url: url,
        dataType: 'jsonp',
        success: function (data) {
            console.log(data);
            data.items.forEach(function (item) {
                let rendered = Mustache.render(template, {
                    image: item.media.m,
                    title: item.title,
                    author: item.author
                });
                news.append(rendered);
            })
        },
        error: function (data) {
            addFlash('danger', "unable to connect to the flickr's API");
        }
    })
}